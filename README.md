# prueba_tecnica_quick


# david restrepo
## prueba tecnica

utilice vuejs, es un framework con el que he trabajado al igual que react, angular tengo proyectos pequeños, en la empresa para la que trabajo solo se enfocan en vuejs y reactjs, solo me hizo falta el filtro por genero

## lib utilizadas
- material design lite
- vuejs
- material_icon



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
